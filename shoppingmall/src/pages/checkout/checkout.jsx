import React, { useContext } from "react";
import { Context } from "../../Context";
import "./checkout.scss";
import CheckoutItem from "../../components/checkout-item/check-out";
export default function CartItem() {
  const { cartItems, total, hidden } = useContext(Context);

  return (
    <div className="checkout-page">
      <div className="checkout-header">
        <div className="header-block">
          <span>Product</span>
        </div>
        <div className="header-block">
          <span>Description</span>
        </div>
        <div className="header-block">
          <span>Quantity</span>
        </div>
        <div className="header-block">
          <span>Price</span>
        </div>
        <div className="header-block">
          <span>Remove</span>
        </div>
      </div>

      {cartItems.map((cartItem) => (
        <CheckoutItem key={cartItem.id} cartItem={cartItem} />
      ))}

      <div className="total">💎 {total}</div>
    </div>
  );
}
